package com.example.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FibonacciTest {
	@Test
	public void testFibo() throws Exception {
		assertEquals(0,Fibonacci.fibo(0));
		assertEquals(1,Fibonacci.fibo(1));
		//assertEquals(-1,Fibonacci.fibo(0));
		assertEquals(0,Fibonacci.fibo(Long.MIN_VALUE));
		assertEquals(233,Fibonacci.fibo(13));
	}
}
