package com.example.demo;

import java.io.FileNotFoundException;

import java.io.UnsupportedEncodingException;

import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {
	
	@GetMapping("/fibonacci")
	public String calculFibo(@RequestParam(name="nombre", required=false, defaultValue="0") String nombre) throws FileNotFoundException, UnsupportedEncodingException {
		int n = Integer.parseInt(nombre) ;
		//System.out.println("poi");
		long result = Fibonacci.fibo(n);
		
		//System.out.println(result);
		
		return /*"F("+n+")="*/""+result;
	}

}
