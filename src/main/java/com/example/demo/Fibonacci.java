package com.example.demo;

public class Fibonacci {
	
	public static long fibo(long n) {
		//System.out.println(n);
	      if (n == 1) {
	        return 1;
	      }
	      else if (n <= 0) {
	        return 0;
	      }
	      else {
	        return fibo(n-2) + fibo(n-1);
	      }
	  }
}
