<center><h1>TP Maven et Intégration continue</h1></center>

<h2>Partie Maven </h2>
Après 5 heures de travail j'ai pu créér une application SpringBoot pour calculer le nombre de Fibonacci correspondant à un entier soumis par l'utilisateur.
Pour exécuter l'application plusieurs possibilité possibles :
- passer par un IDE (Eclipse) en y important le projet Git et en l'exécutant en tant que Java Application. Aller à l'adresse "http://localhost:8080/"
- passer par un terminal, exécuter la commande "./mvnw clean package" puis "java -jar target/Fibonacci-0.0.1-SNAPSHOT.jar". Aller à l'adresse "http://localhost:8080/"
- passer par un terminal, exécuter la commande "curl http://localhost:8080/fibonacci?nombre=x" en remplaçant x par la valeur souhaitée. 

<em><br>Fonctionnement de l'API</br></em>
Les méthodes précédentes lance le main de la classe "FibonacciApplication", qui appelle la classe "Controller" reliée aux pages Web par javascript, et qui fait elle même appel à la classe "Fibonacci" effectuant le calcul.

<em><br>Problèmes rencontrés</br></em>
Quelques problèmes ont été rencontrés à la mise en place du projet, c'est à dire la liaison entre les différentes classes. Mais la majeure partie des problèmes auxquels j'ai pu faire face ont étés liés à la combinaison pages web / projet maven, c'est à dire le déclenchement du calcul depuis la page web.

<h2>Partie Intégration continue</h2>
La partie intégration continue a commencée par le choix de l'outil d'intégration continue. en se basant sur le cours suivit sur ce sujet, j'ai choisi GitLab CI qui me permet d'utiliser le dépôt dispensé par GitLab et en parallèle son "l'extension" CI/CD  sans avoir à installer d'outil supplémentaire.  


Ici les majeures difficultés croisées concernait l'écriture du fichier .gitlab-ci.yml qui met en place l'intégration continue au sein du projet GitLab. En effet, la compréhension du langage utilisé pour ce paramétrage, n'était pas aisée d'autant plus que la documentation avec des exemples clairs sur lesquels se baser ne sont pas nombreuse. Et ce n'est que plus tard que j'ai pu découvrir l'existence d'un template pré-écrit spécialement pour un projet Maven. Mais encore une fois, la compréhension et la mise en place des fonctions manquantes demandées a été difficile, le paramétrage étant basé sur du code pur et dur.
Pour autant, l'intégration a été normalement, menée à bien avec un peu plus de temps que les 5h données, mais sans avoir testé l'intégration d'erreurs.